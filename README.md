# API SWGOH GG

[![dependency status](https://deps.rs/repo/gitlab/swgoh-game/api-swgoh-gg/status.svg)](https://deps.rs/repo/gitlab/swgoh-game/api-swgoh-gg) 
[![pipeline status](https://gitlab.com/swgoh-game/api-swgoh-gg/badges/master/pipeline.svg)](https://gitlab.com/swgoh-game/api-swgoh-gg/commits/master)
[![coverage report](https://gitlab.com/swgoh-game/api-swgoh-gg/badges/master/coverage.svg)](https://gitlab.com/swgoh-game/api-swgoh-gg/commits/master)

Client for [swogh.gg api](https://swgoh.gg/api/)

## Installation

```toml
[dependencies]
api-swgog-gg = "0.2"
```

## Example

This example print all units
```rust
extern crate api_swgoh_gg;

use api_swgoh_gg::get_units;

fn main(){
    let units = get_units().unwrap();
    
    units.iter().foreach(|u| println!("{}", unit.name));
}
```

## Versioning

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html) 
and use [git flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) branching model.

## Roadmap

- [ ] Add get for unit, ships, gear ans abilities
- [ ] Add all guilds routes
- [ ] Add all players routes

## License

Licensed under either of

 * Apache License, Version 2.0
   ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license
   ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.