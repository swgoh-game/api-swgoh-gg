use std::collections::HashMap;

#[derive(Debug, Deserialize)]
pub enum Mark {
    #[serde(alias = "Mk I", alias = "I")]
    MkI,
    #[serde(alias = "Mk II")]
    MkII,
    #[serde(alias = "Mk III")]
    MkIII,
    #[serde(alias = "Mk IV")]
    MkIV,
    #[serde(alias = "Mk V")]
    MkV,
    #[serde(alias = "Mk VI")]
    MkVI,
    #[serde(alias = "Mk VII")]
    MkVII,
    #[serde(alias = "Mk VIII")]
    MkVIII,
    #[serde(alias = "Mk IX")]
    MkIX,
    #[serde(alias = "Mk X")]
    MkX,
    #[serde(alias = "Mk XI")]
    MkXI,
    #[serde(alias = "Mk XII")]
    MkXII,
    #[serde(alias = " ")]
    MkXIIFinisher,
}

#[derive(Debug, Deserialize)]
pub struct Gear {
    //    TODO add ingredients and recipes
    pub base_id: String,
    pub cost: u32,
    pub image: String,
    pub mark: Mark,
    pub name: String,
    pub required_level: u32,
    pub stats: HashMap<u32, f32>,
    pub tier: u32,
    pub url: String,
}
