use crate::model::Alignment;

#[derive(Debug, Deserialize)]
pub struct Ship {
    pub ability_classes: Vec<String>,
    pub activate_shard_count: u64,
    pub alignment: Alignment,
    pub base_id: String,
    pub capital_ship: bool,
    pub categories: Vec<String>,
    pub combat_type: i64,
    pub description: String,
    pub image: String,
    pub name: String,
    pub power: u64,
    pub role: String,
    pub url: String,
}
