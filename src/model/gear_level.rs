#[derive(Debug, Deserialize)]
pub struct GearLevel {
    pub tier: u16,
    #[serde(rename = "gear")]
    pub gears: Vec<String>,
}
