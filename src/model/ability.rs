#[derive(Debug, Deserialize)]
pub struct Ability {
    #[serde(rename = "type")]
    pub ability_type: Option<u32>,
    pub base_id: String,
    pub combat_type: u32,
    pub image: String,
    pub is_zeta: bool,
    pub is_omega: bool,
    pub name: String,
    pub ship_base_id: Option<String>,
    pub tier_max: u32,
    pub url: String,
    #[serde(rename = "character_base_id")]
    pub unit_base_id: Option<String>,
}
