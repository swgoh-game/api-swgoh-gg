use crate::model::gear_level::GearLevel;
use crate::model::Alignment;

#[derive(Debug, Deserialize)]
pub struct Unit {
    pub ability_classes: Vec<String>,
    pub activate_shard_count: u64,
    pub alignment: Alignment,
    pub base_id: String,
    pub categories: Vec<String>,
    pub combat_type: i64,
    pub description: String,
    pub gear_levels: Vec<GearLevel>,
    pub image: String,
    pub name: String,
    pub pk: u64,
    pub power: u64,
    pub role: String,
    pub ship: String,
    pub ship_slot: Option<u64>,
    pub url: String,
}
