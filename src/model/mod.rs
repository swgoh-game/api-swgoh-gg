pub use crate::model::ability::Ability;
pub use crate::model::gear::{Gear, Mark};
pub use crate::model::gear_level::GearLevel;
pub use crate::model::ship::Ship;
pub use crate::model::unit::Unit;

mod ability;
mod gear;
mod gear_level;
mod ship;
mod unit;

/// Unit alignment
#[derive(Debug, Deserialize)]
pub enum Alignment {
    #[serde(rename = "Light Side")]
    LightSide,
    #[serde(rename = "Dark Side")]
    DarkSide,
}
