extern crate log;
extern crate reqwest;
#[macro_use]
extern crate serde;

use model::{Ability, Gear, Ship, Unit};

pub mod model;

const BASE_API_URL: &str = "https://swgoh.gg/api/";

#[macro_export]
macro_rules! get_url {
    ($p:expr) => {
        format!("{}{}/", BASE_API_URL, $p)
    };
    ($p:expr, $i:expr) => {
        format!("{}{}/{}/", BASE_API_URL, $p, $i)
    };
}

#[macro_export]
macro_rules! execute_get {
    ($url: expr) => {
        reqwest::blocking::get(&$url)?.json()
    };
}

pub type Reply<T> = reqwest::Result<T>;

/// Get all abilities
pub fn get_abilities() -> Reply<Vec<Ability>> {
    execute_get!(get_url!("abilities"))
}

/// Get all gears
pub fn get_gears() -> Reply<Vec<Gear>> {
    execute_get!(get_url!("gear"))
}

/// Get a gear with id
pub fn get_gear(id: u64) -> Reply<Gear> {
    execute_get!(get_url!("gear", id))
}

/// Get all ships
pub fn get_ships() -> Reply<Vec<Ship>> {
    execute_get!(get_url!("ships"))
}

/// Get a ship with id
pub fn get_ship(id: u64) -> Reply<Ship> {
    execute_get!(get_url!("ships", id))
}

/// Get all units
pub fn get_units() -> Reply<Vec<Unit>> {
    execute_get!(get_url!("characters"))
}

/// Gat a unit with id
pub fn get_unit(id: u64) -> Reply<Unit> {
    execute_get!(get_url!("characters", id))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn abilities_works() {
        get_abilities().unwrap();
    }

    #[test]
    fn gears_works() {
        get_gears().unwrap();
    }

    #[test]
    fn gear_works() {
        get_gear(1).unwrap();
    }

    #[test]
    fn ships_works() {
        get_ships().unwrap();
    }

    #[test]
    fn ship_works() {
        get_ship(1).unwrap();
    }

    #[test]
    fn units_works() {
        let units = get_units().unwrap();
        let mt: Vec<&Unit> = units.iter().filter(|u| u.pk == 200).collect();

        assert_eq!(mt.len(), 1);
        assert_eq!(&mt[0].name, "Darth Vader");
    }

    #[test]
    fn unit_works() {
        let unit = get_unit(200).unwrap();

        assert_eq!("Darth Vader", &unit.name);
    }
}
