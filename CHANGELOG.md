# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.0] - 2020-02-14
### Changed
- Update dependencies
- Change status from 'actively-developed' to 'passively-maintained'

## [0.2.0] - 2019-08-23
### Added
- Add get gear, unit and ship support. !4 (David Wittwer)

## [0.1.0] - 2019-08-16
### Added
- Create all models and list functions. !1 (David Wittwer)

